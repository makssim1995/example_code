<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main\Loader;
use DS\Development_Layout\Structure\Highload;
use DS\Development_Layout\Structure\Helpers;
use DS\Development_Layout\Structure\Entities;
use Bitrix\Main\Entity\Query as BitrixQuery;
use DS\Development_Layout\Helpers\Project;

if (!Loader::includeModule('ds.development_layout')) {
    ShowError('Не подключен модуль ds.development_layout');

    return;
}

use DS\Development_Layout\Structure\Query as Query;

class FavoritesListComponent extends StandardElementListComponent {
    protected $cacheKeys = ['FLATS', 'PLANS', 'COMMERCIAL'];

    const ENTITY_NAME_TYPE_PLAN_TEMPLATE = [
        'studio' => 'Студия',
        'one_room' => '1-комнатная квартира',
        '2_euro' => '2-евро',
        'one_bedroom' => '2-комнатная квартира',
        '3_euro' => '3-евро',
        'three_bedroom' => '3-комнатная квартира',
        'four_more' => '4+',
    ];

    protected function checkParams()
    {

    }

    public function onPrepareComponentParams($params)
    {
        $result = parent::onPrepareComponentParams($params);
        $result = array_merge($result, array(
            'FAVORITES_FLATS' => ($params['FAVORITES_FLATS'])?$params['FAVORITES_FLATS']:[],
            'FAVORITES_PLANS' => ($params['FAVORITES_PLANS'])?$params['FAVORITES_PLANS']:[],
            'FAVORITES_COMMERCIAL' => ($params['FAVORITES_COMMERCIAL'])?$params['FAVORITES_COMMERCIAL']:[],
            'ORDER' => ($params['ORDER'])?$params['ORDER']:[],
        ));
        return $result;
    }

    public function executeEpilog()
    {
        if (Loader::includeModule("iblock") && $this->arParams['IBLOCK_ID']) {
            if ($GLOBALS['APPLICATION']->GetShowIncludeAreas()) {
                $this->AddIncludeAreaIcons(\CIBlock::GetComponentMenu($GLOBALS['APPLICATION']->GetPublicShowMode(), \CIBlock::GetPanelButtons($this->arParams["IBLOCK_ID"])));
            }
        }

        $GLOBALS['APPLICATION']->SetPageProperty('favorites_header_actions_class', empty($this->arResult['FLATS']) && empty($this->arResult['PLANS']) && empty($this->arResult['COMMERCIAL']) ? 'hidden' : '');
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getResult()
    {
        $favorites_flats = $this->arParams["FAVORITES_FLATS"];
        $favorites_plans = $this->arParams["FAVORITES_PLANS"];
        $favorites_commercial = $this->arParams["FAVORITES_COMMERCIAL"];

        $this->arResult['TYPE_PLAN'] = Helpers::getEnumFieldValues('plan', 'UF_TYPE');

        if($favorites_flats){
            if($this->arParams['ORDER']){
                $queryOrder = [$this->arParams['ORDER']['ORDER_FIELD'] => $this->arParams['ORDER']['ORDER_DIRECTION']];
            }else{
                $queryOrder = ['UF_TOTAL_PRICE' => 'ASC'];
            }
            $query = $this->prepareQueryAdd(
                [
                    'select' => [
                        'ID',
                        'TOTAL_PRICE' => 'UF_TOTAL_PRICE',
                        'POSITIVE_OUTSIDE' => 'UF_POSITIVE_OUTSIDE',
                        'PLAN_' => 'PLAN',
                        'FLOR_' => 'FLOOR',
                        'SECTION_' => 'SECTION',
                        'FLOOR_NUMBER' => 'FLOOR.UF_NUMBER',
                        'SECTION_NUMBER' => 'SECTION.UF_NUMBER',
                        'BUILDING_NUMBER' => 'BUILDING.UF_NUMBER',
                        'PROJECT_' => 'PROJECT',
                    ],
                    'filter' => [
                        'ID' => $favorites_flats,
                    ],
                    'order' => $queryOrder,
                ],
                [
                    'plan' => ['THIS' =>'UF_PLAN', 'REF' => 'ID'],
                    'floor' => ['THIS' =>'UF_FLOOR', 'REF' => 'ID'],
                ],
                'flat'
            );

            $flats = $query->exec()->fetchAll();

            if(!empty($flats))
            {
                foreach ($flats as $flat)
                {
                    $planPicture = '';
                    if($flat['PLAN_UF_IMAGE']){
                        $planPicture = \CFile::GetPath($flat['PLAN_UF_IMAGE']);
                    }
                    $this->arResult['FLATS'][] = [
                        'ID' => $flat['ID'],
                        'PLAN_IMAGE' => $planPicture,
                        'TYPE_PLAN' => $this->arResult['TYPE_PLAN'][$flat['PLAN_UF_TYPE']]['NAME'],
                        'FLOR_NUMBER' => $flat['FLOR_UF_NUMBER'],
                        'SECTION_NUMBER' => $flat['SECTION_UF_NUMBER'],
                        'TOTAL_PRICE' => number_format((int) $flat['TOTAL_PRICE'], 0, '', ' '),
                        'TOTAL_AREA' => number_format($flat['PLAN_UF_TOTAL_AREA'], 1, ',', ''),
                        'BUILDING_NUMBER' => $flat['BUILDING_NUMBER'],
                        'PROJECT_CODE' => $flat['PROJECT_UF_CODE'],
                        'PROJECT_NAME' => $flat['PROJECT_UF_NAME'],
                    ];
                }
            }
        }

        if($favorites_plans){
            $query = $this->prepareQueryAdd(
                [
                    'select' => [
                        'ID',
                        'UF_IMAGE',
                        'UF_CHARACTERISCTIC',
                        'TYPE' => 'UF_TYPE',
                        'RESIDENTS' => 'UF_RESIDENTS',
                        'TOTAL_AREA' => 'UF_TOTAL_AREA',
                        'PLAN_ID' => 'FLAT.UF_PLAN',
                        'COUNT_FLATS' => \Bitrix\Main\Entity\Query::expr()->count('FLAT.ID'),
                        'MIN' => \Bitrix\Main\Entity\Query::expr()->min('FLAT.UF_TOTAL_PRICE'),

                    ],
                    'filter' => [
                        'ID' => $favorites_plans,
                        'FLAT.UF_ACTIVE' => true
                    ],
                    'group' => [
                        'ID'
                    ]
                ],
                [
                    'flat' => ['THIS' =>'ID', 'REF' => 'UF_PLAN'],

                ],
                'plan'
            );

            $plans = $query->exec()->fetchAll();

            if(!empty($plans))
            {
                $this->setPlansProjects();
                foreach ($plans as $plan)
                {
                    $planProjects = $this->arResult['PLANS_PROJECTS'][$plan['PLAN_ID']] ?: [];

                    $moreProjectsCount = count($planProjects) - 3;
                    $moreProjectsFlatsText = '';

                    if ($moreProjectsCount >= 1) {
                        $moreProjects = array_slice($planProjects, 3);
                        $moreFlatsCount = 0;
                        foreach ($moreProjects as $project) {
                            $moreFlatsCount += $project['FLATS_COUNT'];
                        }

                        if ($moreFlatsCount) {
                            $moreProjectsFlatsText = implode(' ', [
                                'И ещё',
                                Russian\pluralize($moreFlatsCount, 'квартира'),
                                'в',
                                Russian\CardinalNumeralGenerator::getCase($moreProjectsCount, 'предложный'),
                                'ЖК',
                            ]);
                        }
                    }



                    $planPicture = '';
                    if($plan['UF_IMAGE']){
                        $planPicture = \CFile::GetPath($plan['UF_IMAGE']);
                    }
                    $this->arResult['PLANS'][$plan['ID']] = [
                        'ID' => $plan['ID'],
                        'TOTAL_AREA' => number_format((double) $plan['TOTAL_AREA'], 2, ',', ''),
                        'CHARACTER' => $plan['UF_CHARACTERISCTIC'],
                        'MIN_PRICE' => number_format((int) $plan['MIN'], 0, '', ' '),
                        'IMAGE' => $planPicture,
                        'PROJECTS' => $planProjects,
                        'MORE_TEXT' => $moreProjectsFlatsText,
                        'TYPE_PLAN' => self::ENTITY_NAME_TYPE_PLAN_TEMPLATE[$this->arResult['TYPE_PLAN'][$plan['TYPE']]['XML_ID']],
                    ];
                }

            }
        }

        if($favorites_commercial){

            $query = $this->prepareQueryAdd(
                [
                    'select' => [
                        'ID',
                        'TOTAL_AREA' => 'UF_TOTAL_AREA',
                        'IMAGE' => 'UF_IMAGE',
                        'BUY_PRICE' => 'UF_BUY_PRICE',
                        'RENT_PRICE' => 'UF_RENT_PRICE',
                        'FLOOR_NUMBER' => 'FLOOR.UF_NUMBER',
                        'SECTION_NUMBER' => 'SECTION.UF_NUMBER',
                        'BUILDING_NUMBER' => 'BUILDING.UF_NUMBER',
                        'PROJECT_' => 'PROJECT',
                    ],
                    'filter' => [
                        'ID' => $favorites_commercial,
                    ],
                ],
                [],
                'commercial'
            );

            $commercial = $query->exec()->fetchAll();

            if(!empty($commercial))
            {
                foreach ($commercial as $elem)
                {
                    $planPicture = '';
                    if($elem['IMAGE']){
                        $planPicture = \CFile::GetPath($elem['IMAGE']);
                    }
                    $this->arResult['COMMERCIAL'][] = [
                        'ID' => $elem['ID'],
                        'PLAN_IMAGE' => $planPicture,
                        'FLOOR_NUMBER' => $elem['FLOOR_NUMBER'],
                        'SECTION_NUMBER' => $elem['SECTION_NUMBER'],
                        'BUY_PRICE' => number_format((int) $elem['BUY_PRICE'], 0, '', ' '),
                        'RENT_PRICE' => number_format((int) $elem['RENT_PRICE'], 0, '', ' '),
                        'TOTAL_AREA' => number_format($elem['TOTAL_AREA'], 1, ',', ''),
                        'BUILDING_NUMBER' => $elem['BUILDING_NUMBER'],
                        'PROJECT_CODE' => $elem['PROJECT_UF_CODE'],
                        'PROJECT_NAME' => $elem['PROJECT_UF_NAME'],
                    ];
                }
            }
        }

        $this->setFlatsTabTitle();
    }

    public function setFlatsTabTitle()
    {
        $flatsProjects = array_column($this->arResult['FLATS'], 'PROJECT_CODE');

        $liveType = Project::defineDominantLiveType($flatsProjects);

        switch ($liveType) {
            case Project::FLAT_TYPE:
                $this->arResult['FLATS_TAB_TITLE'] = 'Квартиры';
                break;
            case Project::APARTMENT_TYPE:
                $this->arResult['FLATS_TAB_TITLE'] = 'Квартиры и апартаменты';
                break;
            case Project::FLAT_APARTMENT_TYPE:
                $this->arResult['FLATS_TAB_TITLE'] = 'Квартиры и апартаменты';
                break;
            default:
                $this->arResult['FLATS_TAB_TITLE'] = 'Квартиры';
        }
    }

    public function getFlats($arPlanid = [])
    {
        if(!empty($arPlanid))
        {
            $query = $this->prepareQueryAdd(
                [
                    'select' => [
                        'ID',
                        'TOTAL_PRICE' => 'UF_TOTAL_PRICE',
                        'PLAN_ID' => 'PLAN.ID',
                        'SECTION_NUMBER' => 'SECTION.UF_NUMBER',
                        'BUILDING_NUMBER' => 'BUILDING.UF_NUMBER',
                        'FLOOR_NUMBER' => 'FLOOR.UF_NUMBER',
                        'PROJECT_CODE' => 'PROJECT.UF_CODE',
                        'PROJECT_ID' => 'PROJECT.ID',
                        'PROJECT_NAME' => 'PROJECT.UF_NAME',

                    ],
                    'filter' => [
                        'UF_ACTIVE' => true,
                        'PLAN.ID' => $arPlanid,
                    ]
                ],
                [
                    'plan' => ['THIS' =>'UF_PLAN', 'REF' => 'ID'],
                ],
                'flat'
            );

            $flats = $query->exec()->fetchAll();

            if(!empty($flats))
            {
                foreach ($flats as $flat) {

                    $flat['TOTAL_PRICE'] = number_format((int) $flat['TOTAL_PRICE'], 0, '', ' ');

                    $this->arResult['PROJECTS'][$flat['PROJECT_ID']]['NAME'] = $flat['PROJECT_NAME'];
                    $this->arResult['PROJECTS'][$flat['PROJECT_ID']]['URL'] = '/projects/' . $flat['PROJECT_CODE'] . '/';
                    $this->arResult['PLANS'][$flat['PLAN_ID']]['PROJECTS'][$flat['PROJECT_ID']][] = $flat;
                    $this->arResult['AJAX_FLATS'][] = $flat;
                    $this->arResult['AJAX_FLATS_ID'][] = $flat['ID'];
                }
            }
        }
    }


    public function setPlansProjects()
    {
        $this->arResult['PLANS_PROJECTS'] = [];

        $query = $this->prepareFlatQuery([
            'select' => [
                'project' => [
                    'UF_NAME',
                    'UF_CODE',
                ],
                'flat' => [
                    'UF_PLAN',
                    'FLATS_COUNT' => BitrixQuery::expr()->count('ID'),
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
            'group' => [
                'project' => [
                    'UF_CODE',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $items = $query->exec()->fetchAll() ?: [];

        if (empty($items)) {
            return;
        }

        foreach ($items as $value) {
            $ufPlan = $value['UF_PLAN'];
            unset($value['UF_PLAN']);

            if (!empty($ufPlan)) {
                $this->arResult['PLANS_PROJECTS'][$ufPlan][] = $value;
            }
        }
    }

    public function getPlanById($arPlanId)
    {
        if(!empty($arPlanId))
        {
            $query = $this->prepareQueryAdd(
                [
                    'select' => [
                        'ID',
                        'UF_IMAGE',
                        'TYPE' => 'UF_TYPE',
                        'RESIDENTS' => 'UF_RESIDENTS',
                        'TOTAL_AREA' => 'UF_TOTAL_AREA',
                        'PLAN_ID' => 'FLAT.UF_PLAN',
                    ],
                    'filter' => [
                        'ID' => $arPlanId,
                    ]
                ],
                [
                    'flat' => ['THIS' =>'ID', 'REF' => 'UF_PLAN'],
                ],
                'plan'
            );

            $plan = $query->exec()->fetch();

            if(!empty($plan))
            {
                $this->arResult['TYPE_PLAN'] = Helpers::getEnumFieldValues('plan', 'UF_TYPE');
                $planPicture = '';
                if($plan['UF_IMAGE']){
                    $planPicture = \CFile::GetPath($plan['UF_IMAGE']);
                }
                $this->arResult['AJAX_PLAN'] = [
                    'ID' => $plan['ID'],
                    'TOTAL_AREA' => number_format((double) $plan['TOTAL_AREA'], 2, ',', ''),
                    'CHARACTER' => $plan['UF_CHARACTERISCTIC'],
                    'MIN_PRICE' => number_format((int) $plan['MIN'], 0, '', ' '),
                    'IMAGE' => $planPicture,
                    'TYPE_PLAN' => self::ENTITY_NAME_TYPE_PLAN_TEMPLATE[$this->arResult['TYPE_PLAN'][$plan['TYPE']]['XML_ID']],
                ];
            }
        }
    }

    public function prepareQueryAdd($data = [], $referenceFields = [], $table)
    {
        $query = Query::prepareQuery($table);

        foreach ($referenceFields as $entity => $thisReference) {
            $query->registerRuntimeField(strtoupper($entity), [
                'data_type' => Highload::getHlClass($entity),
                'reference' => [
                    '=this.' . $thisReference['THIS']  => 'ref.'. $thisReference['REF'],
                ],
            ]);
        }

        if(!empty($data['filter'])){
            $query->setFilter($data['filter']);
        }
        if (!empty($data['select'])) {
            $query->setSelect($data['select']);
        }

        if (!empty($data['order'])) {
            $query->setOrder($data['order']);
        }

        if (!empty($data['group'])) {
            $query->setGroup($data['group']);
        }

        return $query;
    }

    public function prepareFlatQuery($data = [])
    {
        $query = Query::prepareQuery(Entities::FLAT, [
            'select' => $data['select'] ?: [],
            'filter' => array_merge_recursive([
                'flat' => [
                    'UF_ACTIVE' => true,
                ],
            ], $data['filter'] ?: []),
            'referenceFields' => array_merge_recursive([
                'flat' => ['plan' => 'UF_PLAN'],
            ], $data['referenceFields'] ?: []),
            'group' => $data['group'] ?: [],
            'order' => $data['order'] ?: [],
        ]);

        return $query;
    }
}
