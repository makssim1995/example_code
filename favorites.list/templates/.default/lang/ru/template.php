<?php
$MESS['FLATS'] = 'Квартиры';
$MESS['COMMERCIAL'] = 'Коммерческие помещения';
$MESS['PLANS'] = 'Планировки';
$MESS['TABLE_PLAN'] = 'Планировка';
$MESS['TABLE_FLATS'] = 'Комнаты';
$MESS['TABLE_AREA'] = 'Площадь';
$MESS['TABLE_PROJECT'] = 'ЖК';
$MESS['TABLE_SECTION'] = 'Корпус/Секция';
$MESS['TABLE_FLOOR'] = 'Этаж';
$MESS['TABLE_COAST'] = 'Стоимость';
$MESS['CONTINUE_FLAT'] = 'Продолжить выбор';
$MESS['CONTINUE_PLAN'] = 'Продолжить выбор';
$MESS['EMPTY_TEXT'] = 'Здесь будут квартиры, апартаменты, планировки и коммерческие помещения, которые вы добавите в избранное';
$MESS['SELECT_FLAT'] = 'Выбрать квартиру';
$MESS['COMMERCIAL_BUILD'] = 'Коммерческая недвижимость';
