$(document).ready(function(){

    $(document).on('click', '.js-ajaxInfoPlan', function(event){
        event.preventDefault();
        var href =   $(this).data('href'),
            plan_id =   $(this).data('plan_id'),
            targetContainer =   $('#modalCard');
        if (href !== undefined && plan_id !== undefined) {
            $.ajax({
                type: 'POST',
                url: href + '?plan_id=' + plan_id,
                success: function(data){
                    $('#modalCardInner').remove();
                    var findBlock = $(data).find('#modalCardInner');
                    targetContainer.append(findBlock);
                    if (window.innerWidth > 1199) {
                        $('.js-scroll').mCustomScrollbar({
                            mouseWheelPixels: 80,
                            scrollInertia: 0
                        });
                    }
                }
            })
        }
    });

    $(document).on('click', '.js-sortTable', function(event){
        event.preventDefault();
        var $clickedSortHeader = $(this);
        var currentDirection = $clickedSortHeader.attr('data-order-direction');


        $('.js-sortTable').addClass('order-hidden').removeClass('sort-down').addClass('sort-up');
        $('.js-sortTable').attr('data-order-direction', 'ASC');

        $clickedSortHeader.removeClass('order-hidden');

        $clickedSortHeader.removeClass('sort-down sort-up');
        $clickedSortHeader.addClass(currentDirection === 'ASC' ? 'sort-down' : 'sort-up');
        $clickedSortHeader.attr('data-order-direction', currentDirection === 'ASC' ? 'DESC' : 'ASC');

        $('[name="FLATS_ORDER[ORDER_FIELD]"]').val($clickedSortHeader.attr('data-order-field'));
        $('[name="FLATS_ORDER[ORDER_DIRECTION]"]').val($clickedSortHeader.attr('data-order-direction'));

        var $form = $('.hiddenForm');
        var $action = $form.attr('action'),
            $method = $form.attr('method'),
            $dataSerialize = $form.serializeArray();
        $.ajax({
            url: $action,
            data: $dataSerialize,
            method: $method,
            success: function(data) {
                var block = $(data).find('.js-TableInnerFlats').html();
                $('.js-TableInnerFlats').empty();
                $('.js-TableInnerFlats').append(block);
            }
        });
    });
    $(document).on('click', '.js-favoritePlan .favorite-button', function(event){
        setTimeout(function () {
            var $form = $('.hiddenForm');
            var $action = $form.attr('action'),
                $method = $form.attr('method'),
                $dataSerialize = $form.serializeArray();
            $.ajax({
                url: $action,
                data: $dataSerialize,
                method: $method,
                headers: {
                    'Cookie': document.cookie
                },
                success: function(data) {
                    var block = $(data).find('.js-TableInnerFlats').html();
                    $('.js-TableInnerFlats').empty();
                    $('.js-TableInnerFlats').append(block);
                }
            });
        }, 1000);
    });
});