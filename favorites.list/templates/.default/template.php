<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<?if(!empty($arResult['FLATS']) || !empty($arResult['PLANS']) || !empty($arResult['COMMERCIAL'])):?>
    <form action="<?=$component->getPath()?>/orderAjax.php" class="hiddenForm">
        <input type="hidden" name="FLATS_ORDER[ORDER_FIELD]" value="TOTAL_PRICE">
        <input type="hidden" name="FLATS_ORDER[ORDER_DIRECTION]" value="ASC">
    </form>
    <div class="favorites-body">
        <div class="favorites-nav js-tab">
            <div class="apartment-detail__nav">
                <?if(count($arResult['FLATS']) > 0):?>
                    <div class="apartment-detail__nav-elem active js-tab-link js-favorite-nav_block_flat" data-tab-link="#tab-flats">
                        <div class="label js-favorite-nav_flat">
                            <?=count($arResult['FLATS'])?>
                        </div>
                        <?=$arResult['FLATS_TAB_TITLE']?>
                    </div>
                <?endif;?>
                <?if(count($arResult['PLANS']) > 0):?>
                    <div class="apartment-detail__nav-elem <?=(!$arResult['FLATS'])?'active':''?> js-tab-link js-favorite-nav_block_plan" data-tab-link="#tab-plans">
                        <div class="label js-favorite-nav_plan">
                            <?=count($arResult['PLANS'])?>
                        </div>
                        <?=Loc::getMessage('PLANS')?>
                    </div>
                <?endif;?>
                <?if(count($arResult['COMMERCIAL']) > 0):?>
                    <div class="apartment-detail__nav-elem js-tab-link <?=(!$arResult['FLATS'] && !$arResult['PLANS'])?'active':''?>" data-tab-link="#tab-commercial">
                        <div class="label js-favorite-nav_plan">
                            <?=count($arResult['COMMERCIAL'])?>
                        </div>
                        <?=Loc::getMessage('COMMERCIAL')?>
                    </div>
                <?endif;?>
            </div>
        </div>
        <div class="tab-content js-tab-content">
            <?if(count($arResult['FLATS']) > 0):?>
                <div id="tab-flats">
                        <div class="favorites-table">
                            <div class="table apartment-table">
                                <div class="apartment-table__wrap">
                                    <table class="js-TableFlats">
                                        <thead class="hidden-xs">
                                        <tr>
                                            <th>
                                                <?=Loc::getMessage('TABLE_PLAN')?>
                                            </th>
                                            <th>
                                                <div data-order-field="PLAN.UF_ROOMS_COUNT" data-order-direction="ASC" class="js-sortTable sort-header sort-up order-hidden pointer">
                                                    <span class="sort-header__title"><?=Loc::getMessage('TABLE_FLATS')?></span>
                                                    <svg class="icon icon-dropdown-arrow">
                                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                                    </svg>
                                                </div>

                                            </th>
                                            <th>
                                                <div data-order-field="PLAN.UF_TOTAL_AREA" data-order-direction="ASC" class="js-sortTable sort-header sort-up order-hidden pointer">
                                                    <span class="sort-header__title"><?=Loc::getMessage('TABLE_AREA')?>, м<sup>2</sup></span>
                                                    <svg class="icon icon-dropdown-arrow">
                                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                                    </svg>
                                                </div>
                                            </th>
                                            <th><?=Loc::getMessage('TABLE_PROJECT')?></th>
                                            <th><?=Loc::getMessage('TABLE_SECTION')?></th>
                                            <th><?=Loc::getMessage('TABLE_FLOOR')?></th>
                                            <th>
                                                <div data-order-field="UF_TOTAL_PRICE" data-order-direction="ASC" class="js-sortTable sort-header sort-up pointer">
                                                    <span class="sort-header__title"><?=Loc::getMessage('TABLE_COAST')?>,
                                                        <svg class="icon icon-ruble ">
                                                            <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                                        </svg>
                                                    </span>
                                                    <svg class="icon icon-dropdown-arrow">
                                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                                    </svg>
                                                </div>
                                            </th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="js-TableInnerFlats" id="page_favorites_flats">
                                        <? foreach ($arResult['FLATS'] as $flat):?>
                                            <tr class="js-card_flat_<?=$flat['ID']?> js-link" data-url="/projects/<?=$flat['PROJECT_CODE']?>/flat/<?=$flat['ID']?>/">
                                                <td>
                                                    <?if($flat['PLAN_IMAGE']):?>
                                                        <img src="<?=$flat['PLAN_IMAGE']?>" alt="" width="52px" height="40px">
                                                    <?endif;?>
                                                </td>
                                                <td><?=$flat['TYPE_PLAN']?></td>
                                                <td><?=$flat['TOTAL_AREA']?></td>
                                                <td><div class="mobile-label">ЖК</div><span><?=$flat['PROJECT_NAME']?></span></td>
                                                <td><div class="mobile-label">Корпус/Секция</div><span><?=$flat['BUILDING_NUMBER']?>/<?=$flat['SECTION_NUMBER']?></span></td>
                                                <td><div class="mobile-label">Этаж</div><span><?=$flat['FLOR_NUMBER']?></span></td>
                                                <td><div class="mobile-label">
                                                        Стоимость,
                                                        <svg class="icon icon-ruble ">
                                                            <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                                        </svg>
                                                    </div>
                                                    <span><?=$flat['TOTAL_PRICE']?></span>
                                                </td>
                                                <td>
                                                    <a class="favorite-button in_favourite js-favorite-add"
                                                       href="javascript:;"
                                                       data-id="<?=$flat['ID']?>"
                                                       data-type="flat">
                                                        <svg class="icon icon-fav ">
                                                            <use xlink:href="<?=MARKUP_PATH?>/images/sprites.svg#fav"></use>
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <div class="favorites-bottom">
                        <a class="button style_blue" href="/flats/list/">
                            <div class="button__text">
                                <?=Loc::getMessage('CONTINUE_FLAT')?>
                            </div>
                        </a>
                    </div>
                </div>
            <?endif;?>

            <? if(count($arResult['PLANS']) > 0):?>
                <div class="plan-cards__grid" id="tab-plans">
                    <div class="favorites-plans js-gridWrap">
                        <div class="project-grid plan-cards__grid js-grid" id="page_favorites_plans">
                            <? foreach ($arResult['PLANS'] as $plan):?>
                                <div class="project-grid__col js-modalHeight js-card_plan_<?=$plan['ID']?>">
                                    <div class="plan-card">
                                        <div class="plan-card__icon in_favourite js-favorite-add"
                                             data-id="<?=$plan['ID']?>"
                                             data-type="plan">
                                            <svg class="icon icon-fav ">
                                                <use xlink:href="<?=MARKUP_PATH?>/images/sprites.svg#fav"></use>
                                            </svg>
                                        </div>
                                        <?if($plan['IMAGE']):?>
                                            <a class="plan-card__image js-modalCardlLink js-ajaxInfoPlan" href="javascript:;"
                                               data-href="<?=$component->getPath()?>/ajax.php"
                                               data-plan_id="<?=$plan['ID']?>"
                                               data-modal-src="#modalCard">
                                                <img src="<?=$plan['IMAGE']?>" alt="Картинка планировки">
                                            </a>
                                        <?endif;?>
                                        <div class="plan-card__description">
                                            <?if($plan['CHARACTER']):?>
                                                <div class="plan-card__subtitle">
                                                    <?=$plan['CHARACTER']?>
                                                </div>
                                            <?endif;?>
                                            <a class="plan-card__title" href="#">
                                                <?=$plan['TYPE_PLAN']?>
                                            </a>
                                            <?if($plan['TOTAL_AREA']):?>
                                                <div class="plan-card__area">
                                                    <?=$plan['TOTAL_AREA']?> м<sup>2</sup>
                                                </div>
                                            <?endif;?>
                                        </div>
                                        <div class="plan-card__list">
                                            <? foreach ($plan['PROJECTS'] as $key => $project):?>
                                                <a class="plan-card__elem" href="/projects/<?=$project['PROJECT_CODE']?>/" target="_blank">
                                                    <div class="plan-card__elem-item">ЖК <?=$project['PROJECT_NAME']?></div>
                                                    <div class="plan-card__elem-numbers"><?=$project['FLATS_COUNT']?></div>
                                                </a>
                                            <?endforeach;?>
                                        </div>
                                        <?if($plan['MORE_TEXT']):?>
                                            <div class="plan-card__text">
                                                <?=$plan['MORE_TEXT']?>
                                            </div>
                                        <?endif;?>
                                        <div class="plan-card__price">
                                            от <?=$plan['MIN_PRICE']?> ₽
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                    <div class="favorites-bottom">
                        <a class="button style_blue" href="/flats/plan/">
                            <div class="button__text">
                                <?=Loc::getMessage('CONTINUE_PLAN')?>
                            </div>
                        </a>
                    </div>

                    <div class="modal-card js-modalCard" id="modalCard"></div>

                </div>
            <?endif;?>

            <? if(count($arResult['COMMERCIAL']) > 0):?>
                <div class="plan-cards__grid" id="tab-commercial">
                    <div class="favorites-plans">
                        <div class="project-grid plan-cards__grid" id="page_favorites_commercial">

                            <? foreach ($arResult['COMMERCIAL'] as $item):?>

                                <div class="project-grid__col js-card_commercial_<?=$item['ID']?>">
                                    <a class="plan-card" href="/commercial-projects/<?=$item['PROJECT_CODE']?>/commercial/<?=$item['ID']?>/">
                                        <div class="plan-card__icon in_favourite js-favorite-add"
                                             data-id="<?=$item['ID']?>"
                                             data-type="commercial">
                                            <svg class="icon icon-fav ">
                                                <use xlink:href="<?=MARKUP_PATH?>/images/sprites.svg#fav"></use>
                                            </svg>
                                        </div>

                                        <div class="plan-card__image">
                                            <?if($item['PLAN_IMAGE']):?>
                                                <img src="<?=$item['PLAN_IMAGE']?>" alt="Картинка планировки">
                                            <?endif;?>
                                        </div>
                                        <div class="plan-card__description style_border">
                                            <?if($item['TOTAL_AREA']):?>
                                                <div class="plan-card__title">
                                                    <?=$item['TOTAL_AREA']?> м<sup>2</sup>
                                                </div>
                                            <?endif;?>
                                            <div class="plan-card__subtitle">
                                                ЖК <?=$item['PROJECT_NAME']?>
                                            </div>
                                            <div class="plan-card__subtitle"><span>Корпус <?=$item['BUILDING_NUMBER']?></span><span>Секция <?=$item['SECTION_NUMBER']?></span><span>Этаж <?=$item['FLOOR_NUMBER']?></span></div>
                                        </div>
                                        <div class="plan-card__price">
                                            <?if($item['BUY_PRICE'] && $item['RENT_PRICE']):?>
                                                от <?=$item['RENT_PRICE']?> ₽ / мес.
                                            <?elseif ($item['RENT_PRICE']):?>
                                                от <?=$item['RENT_PRICE']?> ₽ / мес.
                                            <?elseif ($item['BUY_PRICE']):?>
                                                от <?=$item['BUY_PRICE']?> ₽
                                            <?endif;?>
                                        </div>
                                        <div class="plan-card__label">

                                            <?if($item['BUY_PRICE'] && $item['RENT_PRICE']):?>
                                                <div class="status-label">аренда</div>
                                                <div class="status-label">продажа</div>
                                            <?elseif ($item['RENT_PRICE']):?>
                                                <div class="status-label">аренда</div>
                                            <?elseif ($item['BUY_PRICE']):?>
                                                <div class="status-label">продажа</div>
                                            <?endif;?>
                                        </div>
                                    </a>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                    <div class="favorites-bottom"><a class="button style_blue" href="/commercial/">
                            <div class="button__text">Продолжить выбор коммерческих помещений</div></a></div>
                </div>
            <?endif;?>
        </div>
    </div>
<?else:?>
    <div class="favorites-empty">
        <div class="favorites-empty__image">
            <img src="<?=MARKUP_PATH?>/images/empty.svg" alt="Картинка сердца">
        </div>
        <div class="favorites-empty__text">
            <?=Loc::getMessage('EMPTY_TEXT')?>
        </div>
        <div class="favorites-empty__buttons">
            <div class="favorites-empty__button">
                <a class="button style_blue" href="/flats/list/">
                    <div class="button__text">
                        <?=Loc::getMessage('SELECT_FLAT')?>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?endif;?>
