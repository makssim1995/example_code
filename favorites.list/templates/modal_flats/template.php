<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
ob_start();
?>
<?if($arResult['AJAX_FLATS']):?>

    <div class="modal-card js-modalCard">
        <div class="modal-card__box" id="modalCardInner">
            <a class="modal-card__close js-modalCardClose" href="javascript:;">
                <svg class="icon icon-close ">
                    <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#close"></use>
                </svg></a>
            <div class="modal-card__aside">
                <div class="modal-card__aside-header">
                    <div class="modal-card__title"><?=$arResult['AJAX_PLAN']['TYPE_PLAN']?></div>
                    <div class="modal-card__subtitle">
                        <?=$arResult['AJAX_PLAN']['TOTAL_AREA']?> кв.м
                    </div>
                </div>
                <?if($arResult['AJAX_PLAN']['IMAGE']):?>
                    <div class="modal-card__image">
                        <img src="<?=$arResult['AJAX_PLAN']['IMAGE']?>" alt="Планировка">
                    </div>
                <?endif;?>
            </div>
            <form class="modal-card__content">
                <div class="modal-card__content-head">
                    <div class="modal-card__content-text"><?=count($arResult['AJAX_FLATS'])?> квартир в этой планировке</div>
                    <div class="modal-card__content-select visible-xs">
                        <div class="form-field">
                            <div class="form-label">Сортировать по</div>
                            <div class="form-control">
                                <select class="form-select js-select2Single" data-placeholder="Выбрать" name="type">
                                    <option value="0" selected>Стоимости</option>
                                    <option value="2">Площади</option>
                                    <option value="3">Этажу</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-card__table js-scroll">
                    <div class="table apartment-table">
                        <table>
                            <thead class="hidden-xs">
                            <tr>
                                <th>ЖК</th>
                                <th>Корпус/Секция</th>
                                <th>Этаж</th>
                                <th>
                                    <div class="sort-header sort-up pointer">
                                        <span class="sort-header__title">Стоимость,
                                            <svg class="icon icon-ruble ">
                                                <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                            </svg>
                                        </span>
                                        <svg class="icon icon-dropdown-arrow ">
                                            <use xlink:href="images/sprites.svg#dropdown-arrow"></use>
                                        </svg>
                                    </div>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($arResult['AJAX_FLATS'] as $item):?>
                                <tr class="js-link" data-url="/projects/<?=$item['PROJECT_CODE']?>/flat/<?=$item['ID']?>/">
                                    <td>

                                            <div class="mobile-label">ЖК</div>
                                            <span>
                                                "<?=$item['PROJECT_NAME']?>"
                                            </span>
                                    </td>
                                    <td>
                                        <div class="mobile-label">Корпус/Секция</div>
                                        <span>
                                            <?=$item['BUILDING_NUMBER']?>/<?=$item['SECTION_NUMBER']?>
                                        </span>
                                    </td>
                                    <td>
                                        <div class="mobile-label">Этаж</div>
                                        <span>
                                            <?=$item['FLOOR_NUMBER']?>
                                        </span>
                                    </td>
                                    <td>
                                        <div class="mobile-label">Стоимость,</div>
                                        <span>
                                        <?=$item['TOTAL_PRICE']?>
                                        <svg class="icon icon-ruble ">
                                            <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                        </svg>
                                    </span>
                                    </td>
                                    <td class="js-favoritePlan">
                                        #FAVORITE_<?=$item['ID']?>#
                                    </td>
                                </tr>
                            <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?endif;?>
<?
$this->__component->arResult['CACHED_TPL'] = @ob_get_contents();
ob_get_clean();
?>
