<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
if(!empty($arResult['AJAX_FLATS_ID'])){
    foreach ($arResult['AJAX_FLATS_ID'] as $item) {
        $arResult['CACHED_TPL'] = preg_replace_callback(
            "/#FAVORITE_$item#/",
            function() use ($APPLICATION, $item, $templateFolder)
            {
                ob_start();
                $APPLICATION->IncludeComponent(
                    "project:favorites.add",
                    "favorite_plans",
                    Array(
                        "ELEMENT_ID" => $item,
                        "TYPE" => 'flat',
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "0",
                    )
                );
                $returnStr = @ob_get_clean();
                return $returnStr;
            },
            $arResult['CACHED_TPL']
        );
    }
}

echo $arResult['CACHED_TPL'];