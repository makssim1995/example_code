<?php
$MESS['FLATS'] = 'Квартиры';
$MESS['COMMERTIAL'] = 'Коммерческие помещения';
$MESS['PLANS'] = 'Планировки';
$MESS['TABLE_PLAN'] = 'Планировка';
$MESS['TABLE_FLATS'] = 'Комнаты';
$MESS['TABLE_AREA'] = 'Площадь';
$MESS['TABLE_PROJECT'] = 'ЖК';
$MESS['TABLE_SECTION'] = 'Корпус/Секция';
$MESS['TABLE_FLOOR'] = 'Этаж';
$MESS['TABLE_COAST'] = 'Стоимость';
$MESS['CONTINUE_FLAT'] = 'Продолжить выбор квартир';
$MESS['CONTINUE_PLAN'] = 'Продолжить выбор планов';
$MESS['EMPTY_TEXT'] = 'Здесь будут квартиры, планировки и коммерческие помещения, которые вы добавите в избранное';
$MESS['SELECT_FLAT'] = 'Выбрать квартиру';
$MESS['COMMERTIAL_BUILD'] = 'Коммерческая недвижимость';
