<?
$MESS['FAVORITES_LIST_IBLOCK_MODULE_NOT_INSTALLED'] = 'Модуль "Инфоблоки" не установлен';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_ID'] = 'ID';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_NAME'] = 'Название';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_SORT'] = 'Индекс сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_ASC'] = 'По возрастанию';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_DESC'] = 'По убыванию';
$MESS['FAVORITES_LIST_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['FAVORITES_LIST_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['FAVORITES_LIST_PARAMETERS_SHOW_NAV'] = 'Постраничная навигация';
$MESS['FAVORITES_LIST_PARAMETERS_COUNT'] = 'Количество элементов';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_FIELD1'] = 'Поле первой сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_DIRECTION1'] = 'Направление первой сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_FIELD2'] = 'Поле второй сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_SORT_DIRECTION2'] = 'Направление второй сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_SECTION_CODE'] = 'Код раздела';
$MESS['FAVORITES_LIST_PARAMETERS_GROUP_SORT_NAME'] = 'Параметры сортировки';
$MESS['FAVORITES_LIST_PARAMETERS_IBLOCK_CITY_ID'] = 'Код инфобока с городами';
$MESS['FAVORITES_LIST_PARAMETERS_IBLOCK_CITY_TYPE'] = 'Тип инфоблока с городами';
?>