<?php

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

define('AJAX_REQUEST', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

if (!AJAX_REQUEST) {
    die();
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->toArray();

if ($request['FLATS_ORDER']) {
    $APPLICATION->IncludeComponent(
        "project:favorites.list",
        "",
        Array(
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "0",
            "ORDER" => $request['FLATS_ORDER'],
            'FAVORITES_FLATS' => unserialize(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("favorites_flat")),
            'FAVORITES_PLANS' => unserialize(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("favorites_plan")),
        )
    );
}

