<?php

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('BX_BUFFER_USED', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';


class ComponentAjax
{
    public function handle()
    {
        $mainComponentClass = \CBitrixComponent::includeComponentClass('project:favorites.list');
        $mainComponent = new $mainComponentClass;
        $mainComponent->initComponent('project:favorites.list');
        $mainComponent->setTemplateName('modal_flats');
        $planId = $mainComponent->getRequest()->getQuery('plan_id');

        $mainComponent->getFlats($planId);
        $mainComponent->getPlanById($planId);

        $mainComponent->IncludeComponentTemplate();
        $mainComponent->SetResultCacheKeys(['CACHED_TPL' , 'AJAX_FLATS_ID']);;
    }
}

$componentAjax = new ComponentAjax();
$componentAjax->handle();
